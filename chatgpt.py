import openai

class ChatGPT:
    def __init__(self, api_key):
        self.openai = openai
        #self.openai.organization ="Vietnam News Agency"
        self.openai.api_key = api_key
        self.messages = []

    def send_request(self, prompt, max_tokens=10000, temperature=1.00):
        try:
            self.messages.append({'role': 'user', 'content': prompt})
            response = self.openai.ChatCompletion.create(
                model='gpt-3.5-turbo',
                max_tokens=max_tokens,
                temperature=temperature,
                messages=self.messages
            )
            self.messages.append({'role': 'assistant', 'content': response.choices[0].message.content})
            return {'usage': response.usage.total_tokens, 'content': response.choices[0].message.content}
        except Exception as e:
            return {'error': e}
        
    def send_cmd(self, prompt, max_tokens=10000, temperature=1.00):
        try:
            self.messages.append({prompt})
            response = self.openai.Completion.create(
                model='gpt-3.5-turbo',
                prompt=self.messages,
                temperature=temperature,
                max_tokens=max_tokens,
                top_p=1.0,
                frequency_penalty=0.0,
                presence_penalty=0.0
            )
            self.messages.append({response.choices[0].message.content})
            return {'usage': response.usage.total_tokens, 'content': response.choices[0].message.content}
        except Exception as e:
            return {'error': e}